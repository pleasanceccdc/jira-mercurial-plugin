jQuery(function($) {
    var mercurial = {
        getContextPath : function() {
            return contextPath;
        },

        getParamsFromFieldSet : function(fieldSet) {
            var params = {};

            fieldSet.find("input").each(function() {
                params[this.name] = this.value;
            });

            return params;
        },

        initShowMoreButton : function(moreButton, moreUrlPath, dataType, callback) {
            var moreButtonContainer = moreButton.parent();
            var params = this.getParamsFromFieldSet(moreButtonContainer.children("fieldset"));

            moreButton.click(function() {
                moreButton.hide();
                moreButtonContainer.append("<img src='" + mercurial.getContextPath() + "/images/icons/wait.gif'/>");
                
                $.ajax({
                    type : "GET",
                    url : mercurial.getContextPath() + moreUrlPath,
                    dataType: dataType,
                    data : params,
                    success : function(someHtml) {
                        moreButtonContainer.remove();
                        callback(someHtml);
                    }
                });
            });
        },

        createTemporaryInvisibleDiv : function(parent) {
            $("<div class='plugin_mercurial_temp' style='display: none;'/>").appendTo(parent);
            return parent.children("div.plugin_mercurial_temp");
        },

        initShowMoreButtonInIssueTab : function(moreButton) {
            var currentIssue = $("a[id^='issue_key_']");
            var currentIssueKey = $.trim(currentIssue.text());

            if (currentIssue.length == 1) {
                moreButton.parent().children("fieldset").find("input[name='issueKey']").attr("value", $.trim(currentIssueKey));
            }

            this.initShowMoreButton(
                moreButton,
                "/browse/" + currentIssueKey,
                "html",
                function(html) {
                    var issueActionsContainer = $("#issue_actions_container");
                    var tempCommitsDiv = mercurial.createTemporaryInvisibleDiv($("body"));

                    tempCommitsDiv.html(html);
                    tempCommitsDiv.find("div.issuePanelContainer table, div.issuePanelContainer div.plugin_mercurial_showmore_issuetab").appendTo(issueActionsContainer);
                    tempCommitsDiv.remove();

                    mercurial.initShowMoreButtonInIssueTab(issueActionsContainer.find("input.plugin_mercurial_showmore_issuetab_button"));
                }
            );
        },

        initShowMoreButtonInProjectTab : function(moreButton) {
            var selectedVersion = $("select[name='selectedVersion'] option:selected");
            var buttonFieldSet = moreButton.parent().children("fieldset");
            var params = this.getParamsFromFieldSet(buttonFieldSet);

            if (selectedVersion.length == 1) {
                buttonFieldSet.find("input[name='versionId']").attr("value", selectedVersion.attr("value"));
            }

            this.initShowMoreButton(
                moreButton,
                "/browse/" + params["projectKey"],
                "json",
                function(jsObject) {
                    var projectCommitsContainer = $("div.projectPanel table.plugin_mercurial_projectcommits_table");
                    var tempCommitsDiv = mercurial.createTemporaryInvisibleDiv($("body"));

                    tempCommitsDiv.html(jsObject["content"]);

                    var commitsHtml = tempCommitsDiv.find("table.plugin_mercurial_projectcommits_table");
                    var commitsHtmlTableBody = commitsHtml.children("tbody");
                    
                    if (commitsHtmlTableBody.length > 0)
                        commitsHtml = commitsHtmlTableBody;

                    commitsHtml.appendTo(projectCommitsContainer);

                    tempCommitsDiv.remove();

                    mercurial.initShowMoreButtonInProjectTab(projectCommitsContainer.find("input.plugin_mercurial_showmore_projectab_button"));
                }
            );
        },

        initVersionSelectForm : function(theForm) {
            theForm.find("select[name='selectedVersion']").change(function() {
                theForm.submit();
            });
        }
    };

    $("input.plugin_mercurial_showmore_issuetab_button").each(function() {
        mercurial.initShowMoreButtonInIssueTab($(this));
    });

    $("input.plugin_mercurial_showmore_projectab_button").each(function() {
        mercurial.initShowMoreButtonInProjectTab($(this));
    });

    mercurial.initVersionSelectForm($("form.plugin_mercurial_versionselect_form"));
});

// Create a string parameter of all the REST parameters with the correct names
function addRestParams(currentobj) {
    var rest_repo = document.getElementsByName('rest_repo')[0];
    // TODO should be able to use repo id here instead
    var params = "repo=" + rest_repo.options[rest_repo.selectedIndex].text;
    
    // Input field name to REST API name mapping
    var textfieldnames = {'rest_artifactid': 'artifactid',
                          'rest_start': 'start',
                          'rest_end': 'end',
                          'rest_email': 'email',
                          'rest_addressedinfieldname': 'addressedinfieldname' };
    
    for (fieldname in textfieldnames) {
        var fieldObj = document.getElementsByName(fieldname)[0];
        var value = fieldObj.value;
        if (value != '') {
            params = params + "&" + textfieldnames[fieldname] + "=" + value;
        }
    }

    var newurl = currentobj.href + "?" + params;
    window.location.href = newurl;
    // Tell the href not to use the default
    return false;
}

