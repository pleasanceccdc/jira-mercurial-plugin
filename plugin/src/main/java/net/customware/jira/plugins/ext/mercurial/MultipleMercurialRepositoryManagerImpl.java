package net.customware.jira.plugins.ext.mercurial;

import net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexService;
import net.customware.jira.plugins.ext.mercurial.revisions.RevisionIndexer;
import net.customware.hg.util.ExecUtil;

import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.InfrastructureException;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.propertyset.DefaultJiraPropertySetFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.service.ServiceManager;

import com.opensymphony.module.propertyset.PropertySet;
import org.apache.log4j.Logger;

import java.util.*;

import java.io.File;
import java.io.IOException;

/**
 * This is a wrapper class for many MercurialManagers.
 * Configured via {@link HgPropertiesLoader#PROPERTIES_FILE_NAME}
 *
 * @author Dylan Etkin
 * @see {@link net.customware.jira.plugins.ext.mercurial.MercurialManager}
 */
public class MultipleMercurialRepositoryManagerImpl implements MultipleMercurialRepositoryManager
{
    private static Logger log = Logger.getLogger(MultipleMercurialRepositoryManagerImpl.class);

    public static final String APP_PROPERTY_PREFIX = "jira.plugins.mercurial";

    public static final String REPO_PROPERTY = "jira.plugins.mercurial.repo";

    public static final String LAST_REPO_ID = "last.repo.id";

    public static final long FIRST_REPO_ID = 1;

    private PropertySet pluginProperties;

    private RevisionIndexer revisionIndexer;

    private Map<Long, MercurialManager> managerMap = new HashMap<Long, MercurialManager>();

    private final JiraPropertySetFactory jiraPropertySetFactory;

    private long lastRepoId;

    public MultipleMercurialRepositoryManagerImpl()
    {
        this.jiraPropertySetFactory = new DefaultJiraPropertySetFactory();

        managerMap = loadHgManagers();

        // Always create a revision indexer and do it after we know we
        // have succeed in initializing our repositories.
        // This means that the service will always exist.
        revisionIndexer = new RevisionIndexer(this);
    }

    /**
     * Loads a {@link java.util.Map} of {@link net.customware.jira.plugins.ext.mercurial.MercurialManager} IDs to the {@link com.atlassian.jira.plugin.ext.mercurial.MercurialManager}.
     * The repositories are loaded from persistent storage.
     *
     * @return A map of {@link net.customware.jira.plugins.ext.mercurial.MercurialManager} IDs to the {@link com.atlassian.jira.plugin.ext.mercurial.MercurialManager}.
     * @throws InfrastructureException Thrown if there's a problem loading repositories configuration from the plugin's configuration file.
     */
    Map<Long, MercurialManager> loadHgManagers()
    {

        Map<Long, MercurialManager> managers = loadManagersFromJiraProperties();

        if (managers.isEmpty())
        {
            log.warn("No Mercurial repositories configured yet");
        }

        return managers;
    }

    /**
     * The Mercurial configuration properties are stored in the application properties.  It's not the best place to
     * store collections of information, like multiple repositories, but it will work.  Keys for the properties look
     * like:
     * <p/>
     * <tt>jira.plugins.mercurial.&lt;repoId&gt;;&lt;property name&gt;</tt>
     * <p/>
     * Using this scheme we can get all the properties and put them into buckets corresponding to the <tt>repoId</tt>.
     * Then when we have all the properties we can go about building the {@link net.customware.jira.plugins.ext.mercurial.MercurialProperties}
     * objects and creating our {@link net.customware.jira.plugins.ext.mercurial.MercurialManager}s.
     *
     * @return A {@link java.util.Map} of {@link net.customware.jira.plugins.ext.mercurial.MercurialManager} IDs to the {@link com.atlassian.jira.plugin.ext.mercurial.MercurialManager}.
     *         loaded from JIRA's application properties.
     */
    private Map<Long, MercurialManager> loadManagersFromJiraProperties()
    {

        pluginProperties = jiraPropertySetFactory.buildCachingDefaultPropertySet(APP_PROPERTY_PREFIX, true);

        lastRepoId = pluginProperties.getLong(LAST_REPO_ID);

        // recreate the MercurialManagers
        Map<Long, MercurialManager> managers = new LinkedHashMap<Long, MercurialManager>();
        for (long i = FIRST_REPO_ID; i <= lastRepoId; i++)
        {
            MercurialManager mgr = createManagerFromPropertySet(i, jiraPropertySetFactory.buildCachingPropertySet(REPO_PROPERTY, i, true));
            if (mgr != null)
                managers.put(i, mgr);
        }
        return managers;
    }

    /**
     * Recreate a manager from a loaded property set.
     */
    MercurialManager createManagerFromPropertySet(long index, PropertySet properties)
    {
        try
        {
            if (properties.getKeys().isEmpty())
                return null;

            return new MercurialManagerImpl(index, properties, properties.getBoolean(MultipleMercurialRepositoryManager.HG_SUBREPOS_KEY));
        }
        catch (IllegalArgumentException e)
        {
            log.error("Error creating MercurialManager " + index + ". Probably was missing a required field (e.g., repository name or root). Skipping it.", e);
            return null;
        }
    }

    /**
     * Create a new manager along with the cached property set for its
     * parameters.
     */
    public MercurialManager createRepository(HgProperties properties, boolean isSubrepos)
    {
        long repoId; // not actually the repository UUID
        synchronized (this)
        {
            repoId = ++lastRepoId;
            pluginProperties.setLong(LAST_REPO_ID, lastRepoId);
        }

        PropertySet set = jiraPropertySetFactory.buildCachingPropertySet(REPO_PROPERTY, repoId, true);

        MercurialManager mercurialManager = new MercurialManagerImpl(repoId, HgProperties.Util.fillPropertySet(properties, set), isSubrepos);

        managerMap.put(mercurialManager.getId(), mercurialManager);

        if (!isSubrepos) {
            if (isIndexingRevisions()) {
                getRevisionIndexer().addRepository(mercurialManager);
            }
        } else {
            checkSubrepos(mercurialManager);
        }

        return mercurialManager;
    }

    public void checkSubrepos(MercurialManager mercurialManager) {
        log.info("Checking the list of subrepositories");
        ArrayList<String> currentSubrepos = listSubrepos(mercurialManager.getSubreposcmd());
        for (Iterator it = currentSubrepos.iterator(); it.hasNext(); ) {
            String repoName = (String)it.next();
            
            // This is a URI not a directory
            String newRoot = mercurialManager.getRoot() + "/" + repoName;
            log.debug("Checking subrepository: " + newRoot);
            
            boolean isNewRepo = true;
            // TODO replace this O(N2) with another map
            for (Iterator it2 = managerMap.values().iterator(); it2.hasNext(); ) {
                MercurialManager x = (MercurialManager)it2.next();
                // TODO use UUID or define equals?
                if (x.getRoot().equals(newRoot)) {
                    isNewRepo = false;
                    break;
                }
            }

            if (isNewRepo) {
                log.warn("Found a new repository: " + repoName);
                MercurialProperties mp = new MercurialProperties();
                // Get the default values
                mp = HgProperties.Util.fillHgProperties(mercurialManager.getProperties(), mp);
                mp.setSubrepos(false);
                
                // Make the unique changes for each repository
                mp.setRoot(newRoot);
                mp.setDisplayName(repoName); // This may not be unique
                //mp.setClonedir(mp.getClonedir() + dir.sep + repoName);
                mp.setRevisionIndexing(true);
                mp.setChangeSetFormat(mp.getChangesetFormat().replace("${repo}", repoName));
                mp.setFileModifiedFormat(mp.getFileModifiedFormat().replace("${repo}", repoName));
                
                // The false prevents recursion
                createRepository(mp, false);
            }

            // Repositories that disappear will be marked as inactive
            // but not deleted
        }            
    }

    /**
     * Get the list of repositories with a command such as
     * ssh hg ls ../repos
     */
    private ArrayList<String> listSubrepos(String cmd) {
		ArrayList<String> result = new ArrayList<String>();
		
		// HengHwa - JIRA 5.x
		// Check empty command before proceed
		if (cmd != null && !cmd.isEmpty() && !cmd.trim().isEmpty()) {
			ArrayList<String> linesList;
			String[] env_vars = new String[]{};
			ExecUtil eu = new ExecUtil();
			
			try {
				linesList = eu.exec(cmd, env_vars, new File("/"), log);
				String[] lines = linesList.toArray(new String[linesList.size()]);
				int i = 0;
			
				while(i < lines.length) {
					log.debug("Parsing line " + i + " : " + lines[i]);
					i++;
				}
				result = linesList;// TODO
			} catch (IOException ioe) {
				log.error("unable to run cmd: " + cmd);
			}
		} else {
			log.warn("Empty command");
		}
		return result;
    }

    public MercurialManager updateRepository(long repoId, HgProperties properties)
    {
        MercurialManager mercurialManager = getRepository(repoId);
        mercurialManager.update(properties);
        return mercurialManager;
    }

    public void reindexRepository(long repoId) throws IndexException, IOException
    {
        MercurialManager mercurialManager = getRepository(repoId);
        // TODO add a method to clear the index from the cache first?
        revisionIndexer.checkAndUpdateOneIndex(mercurialManager);
    }

    public void removeRepository(long repoId)
    {
        MercurialManager original = managerMap.get(repoId);
        if (original == null)
        {
            return;
        }
        try
        {
            managerMap.remove(repoId);

            // Would like to just call remove() but this version
            // doesn't appear to have that, remove all of it's
            // properties instead
            for (String key : new ArrayList<String>(original.getProperties().getKeys()))
                original.getProperties().remove(key);

            if (getRevisionIndexer() != null)
                getRevisionIndexer().removeEntries(original);
        }
        catch (Exception e)
        {
            throw new InfrastructureException("Could not remove repository index", e);
        }
    }

    public boolean isIndexingRevisions()
    {
        return getRevisionIndexer() != null;
    }

    public RevisionIndexer getRevisionIndexer()
    {
        return revisionIndexer;
    }

    /**
     * All repositories of any kind or state.
     */    
    public Collection<MercurialManager> getRepositoryList()
    {
        // Note: the values Collection is backed by the LinkedHashMap
        // so there is a rare risk of concurrent modification in the indexer.
        return managerMap.values();
    }

    /**
     * All subrepositories whether active or not
     */
    public Collection<MercurialManager> getSubrepositoriesList()
    {
        Collection result = new HashSet();
        for (Iterator it = managerMap.values().iterator(); it.hasNext(); ) {
            MercurialManager m = (MercurialManager)it.next();
            if (m.isSubrepos()) {
                result.add(m);
            }
        }
        return result;
    }

    /**
     * All active repositories but no subrepositories whether active or not
     */
    public Collection<MercurialManager> getActiveRepositoryList()
    {
        return getRepositoryList(true);
    }

    /**
     * All inactive repositories but no subrepositories whether inactive or not
     */
    public Collection<MercurialManager> getInactiveRepositoryList()
    {
        return getRepositoryList(false);
    }

    private Collection<MercurialManager> getRepositoryList(boolean active)
    {
        Collection result = new HashSet();
        for (Iterator it = managerMap.values().iterator(); it.hasNext(); ) {
            MercurialManager m = (MercurialManager)it.next();
            if (m.isSubrepos()) {
                continue;
            }
            if (m.isActive()) {
                if (active) {
                    result.add(m);
                }
            } else {
                if (!active) {
                    result.add(m);
                }
            }
        }
        return result;
    }

    public MercurialManager getRepository(long id)
    {
        return managerMap.get(id);
    }

    /**
     * @return the first repository with the given name, or null if none exists
     */
    public MercurialManager getRepository(String repoName)
    {
        for (Iterator it = managerMap.values().iterator(); it.hasNext(); ) {
            MercurialManager m = (MercurialManager)it.next();
            String name = m.getDisplayName();
            if (name != null && name.equals(repoName)) {
                return m;
            }
        }
        return null;
    }

    void startRevisionIndexer()
    {
        getRevisionIndexer().start();
    }

    public void start()
    {
        try
        {
            if (isIndexingRevisions())
            {
                startRevisionIndexer();
            }
        }
        catch (InfrastructureException ie)
        {
            /* Log error, don't throw. Otherwise, we get issue SVN-234 */
            log.error("Error starting " + getClass(), ie);
        }
    }
}
