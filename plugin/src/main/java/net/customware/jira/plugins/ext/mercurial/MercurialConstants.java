package net.customware.jira.plugins.ext.mercurial;

/**
 * Letters indicating changes in the Mercurial repository. Refer to
 * <a href="http://hgbook.red-bean.com/read/mercurial-in-daily-use.html">the Mercurial book</a>.
 */
public interface MercurialConstants
{
    char MODIFICATION = 'M';
    char ADDED = 'A';
    char DELETED = 'D';
    char REPLACED = 'R';
}
