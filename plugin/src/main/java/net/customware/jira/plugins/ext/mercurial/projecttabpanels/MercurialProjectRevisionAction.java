package net.customware.jira.plugins.ext.mercurial.projecttabpanels;

import net.customware.hg.core.io.HGLogEntry;
import net.customware.jira.plugins.ext.mercurial.MultipleMercurialRepositoryManager;
import net.customware.jira.plugins.ext.mercurial.issuetabpanels.changes.MercurialRevisionAction;

import com.atlassian.jira.plugin.projectpanel.ProjectTabPanelModuleDescriptor;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import java.util.Map;

import org.ofbiz.core.util.UtilMisc;

/**
 * One item in the 'Mercurial Commits' project tab.
 *
 * This class extends {@link MercurialRevisionAction} (basically, there is no issue to group by here,
 * and we need to use a ProjectTabPanelModuleDescriptor in stead of an IssueTabPanelModuleDescriptor)
 */
public class MercurialProjectRevisionAction extends MercurialRevisionAction
{
    protected final ProjectTabPanelModuleDescriptor projectDescriptor;

    public MercurialProjectRevisionAction(HGLogEntry logEntry,
                                           MultipleMercurialRepositoryManager multipleMercurialRepositoryManager,
                                           ProjectTabPanelModuleDescriptor descriptor, long repoId)
    {
        super(logEntry, multipleMercurialRepositoryManager, null, repoId);
        this.projectDescriptor = descriptor;
    }

    public String getHtml(JiraWebActionSupport webAction)
    {
        Map params = UtilMisc.toMap("webAction", webAction, "action", this);
        return descriptor.getHtml("view", params);
    }
}
